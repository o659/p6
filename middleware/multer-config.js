const multer = require("multer")

const MIME_TYPES = {
  "image/jpg": "jpg",
  "image/jpeg": "jpg",
  "image/png": "png"
}

//Preventing conflict with MIME_TYPES(client side)
const fileFilter = (req, file, cb) => {
  if (MIME_TYPES[file.mimetype] === undefined) {
    cb(new Error("file.mimetype"), false)
  }
  else {
    cb(null, true)
  }
}

//Destination & Filename
const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, "images")
  },
  filename: (req, file, callback) => {
    const tmpName = file.originalname.split(/\s+/).join("_")
    const extension = MIME_TYPES[file.mimetype]
    const name = tmpName.replace(`.${extension}`, "")
    callback(null, `${name}${Date.now()}.${extension}`)
  }
})

module.exports = multer({
  storage: storage,
    limits: { fileSize: 1024 * 1024 * 4
  },
    fileFilter: fileFilter
  }).single("image")