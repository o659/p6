const userToken = require("../utils/utils")
const jwt = require("jsonwebtoken")

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1]
    const decodedToken = jwt.verify(token, userToken)
    const userId = decodedToken.userId
    if (req.body.userId && req.body.userId !== userId) {
      throw "Invalid credential"
    }
    else {
      next()
    }
  } catch (error) {
    res.status(401).json({
      error: error || new Error("Invalid request")
    })
  }
}