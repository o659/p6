const express = require("express")
const router = express.Router()
const sauceCtrl = require("../controllers/sauce")
const auth = require("../middleware/auth")
const multer = require("../middleware/multer-config")

router.get("/", auth, sauceCtrl.viewingSauces)
router.post("/", auth, multer, sauceCtrl.creatingSauce)
router.get("/:id", auth, sauceCtrl.viewingOneSauce)
router.post("/:id/like", auth, sauceCtrl.likingDislikingSauce)
router.put("/:id", auth, multer, sauceCtrl.updatingSauce)
router.delete("/:id", auth, sauceCtrl.deletingSauce)

module.exports = router