require("dotenv").config()
const express = require("express")
const mongoose = require("mongoose")
const path = require("path")
const app = express()

mongoose.connect(process.env.DATABASE_URL)
  .then(() => console.log("connection to mongodb succeeded"))
  .catch(() => console.log("connection to mongodb failed"))

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*")
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content, Accept, Content-Type, Authorization")
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH, OPTIONS")
  next()
})

app.use(express.json())

app.use("/images", express.static(path.join(__dirname, "images")))

const authRoutes = require("./routes/auth")
app.use("/api/auth", authRoutes)
const sauceRoutes = require("./routes/sauce")
app.use("/api/sauces", sauceRoutes)

module.exports = app