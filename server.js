require("dotenv").config()
const port = process.env.SERVER_PORT
const http = require("http")
const app = require("./app")
const server = http.createServer(app)
server.listen(port, () => console.log(`Server listening on port ${port}`))
