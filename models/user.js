const mongoose = require("mongoose")
const uniqueValidator = require("mongoose-unique-validator")
//Method Schema create a data model
const userSchema = mongoose.Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true }
})
//Mongoose unique validator
userSchema.plugin(uniqueValidator)
//Method model transform the schema in a usable schema
module.exports = mongoose.model("User", userSchema)